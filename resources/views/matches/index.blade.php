@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        {{ __('Match List') }}
                        <a href="{{ route('matches.create') }}" class="btn btn-primary float-right">{{ __('Add Match') }}</a>
                    </div>

                    <div class="card-body">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>{{ __('Home Club') }}</th>
                                    <th>{{ __('Home Score') }}</th>
                                    <th>{{ __('Away Score') }}</th>
                                    <th>{{ __('Away Club') }}</th>
                                    <th>{{ __('Match Date') }}</th>
                                    <th>{{ __('Actions') }}</th>
                                </tr>
                            </thead>

                            <tbody>
                                @foreach ($matches as $match)
                                    <tr>
                                        <td>{{ $match->clubHome->name }}</td>
                                        <td>{{ $match->score_home }}</td>
                                        <td>{{ $match->score_away }}</td>
                                        <td>{{ $match->clubAway->name }}</td>
                                        <td>{{ $match->match_date }}</td>
                                        <td>
                                            <a href="{{ route('matches.edit', $match->id) }}" class="btn btn-primary">{{ __('Edit') }}</a>

                                            <form action="{{ route('matches.destroy', $match->id) }}" method="POST" style="display: inline-block;">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-danger">{{ __('Delete') }}</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
