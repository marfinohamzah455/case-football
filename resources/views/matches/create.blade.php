<!-- resources/views/matches/create.blade.php -->

@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-header">{{ __('Input Skor') }}</div>

        <div class="card-body">
            <form method="POST" action="{{ route('matches.store') }}">
                @csrf

                <div class="form-group">
                    <label for="home_club_id">{{ __('Klub Tuan Rumah') }}</label>
                    <select id="home_club_id" class="form-control @error('home_club_id') is-invalid @enderror" name="home_club_id" required>
                        <option value="">-- Pilih Klub Tuan Rumah --</option>
                        @foreach ($clubs as $club)
                            <option value="{{ $club->id }}" @if (old('home_club_id') == $club->id) selected @endif>{{ $club->name }}</option>
                        @endforeach
                    </select>
                    @error('home_club_id')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="away_club_id">{{ __('Klub Tamu') }}</label>
                    <select id="away_club_id" class="form-control @error('away_club_id') is-invalid @enderror" name="away_club_id" required>
                        <option value="">-- Pilih Klub Tamu --</option>
                        @foreach ($clubs as $club)
                            <option value="{{ $club->id }}" @if (old('away_club_id') == $club->id) selected @endif>{{ $club->name }}</option>
                        @endforeach
                    </select>
                    @error('away_club_id')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</
