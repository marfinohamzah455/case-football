@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        {{ __('Edit Match') }}
                    </div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('matches.update', $match->id) }}">
                            @csrf
                            @method('PUT')

                            <div class="form-group">
                                <label for="club_home">{{ __('Home Club') }}</label>
                                <select name="club_home" id="club_home" class="form-control">
                                    @foreach ($clubs as $club)
                                        <option value="{{ $club->id }}" @if ($match->club_home_id == $club->id) selected @endif>{{ $club->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="club_away">{{ __('Away Club') }}</label>
                                <select name="club_away" id="club_away" class="form-control">
                                    @foreach ($clubs as $club)
                                        <option value="{{ $club->id }}" @if ($match->club_away_id == $club->id) selected @endif>{{ $club->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="score_home">{{ __('Home Score') }}</label>
                                <input id="score_home" type="number" class="form-control @error('score_home') is-invalid @enderror" name="score_home" value="{{ $match->score_home }}" required autocomplete="score_home">

                                @error('score_home')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="score_away">{{ __('Away Score') }}</label>
                                <input id="score_away" type="number" class="form-control @error('score_away') is-invalid @enderror" name="score_away" value="{{ $match->score_away }}" required autocomplete="score_away">

                                @error('score_away')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="match_date">{{ __('Match Date') }}</label>
                                <input id="match_date" type="date" class="form-control @error('match_date') is-invalid @enderror" name="match_date" value="{{ $match->match_date }}" required autocomplete="match_date">

                                @error('match_date')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <button type="submit" class="btn btn-primary">{{ __('Update Match') }}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
