@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header d-flex justify-content-between align-items-center">
                        {{ __('Club List') }}
                        <a href="{{ route('clubs.create') }}" class="btn btn-primary">{{ __('Add Club') }}</a>
                    </div>

                    <div class="card-body">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>{{ __('No') }}</th>
                                    <th>{{ __('Club Name') }}</th>
                                    <th>{{ __('Club City') }}</th>
                                    <th>{{ __('Action') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($clubs as $club)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $club->name }}</td>
                                        <td>{{ $club->city }}</td>
                                        <td>
                                            <div class="d-flex justify-content-between align-items-center">
                                                <a href="{{ route('clubs.show', $club->id) }}" class="btn btn-primary mr-2">{{ __('Show') }}</a>
                                                <a href="{{ route('clubs.edit', $club->id) }}" class="btn btn-secondary mr-2">{{ __('Edit') }}</a>
                                                <form method="POST" action="{{ route('clubs.destroy', $club->id) }}">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete this club?')">{{ __('Delete') }}</button>
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
