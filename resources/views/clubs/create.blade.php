<!-- resources/views/clubs/create.blade.php -->

@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-header">{{ __('Input Data Klub') }}</div>

        <div class="card-body">
            <form method="POST" action="{{ route('clubs.store') }}">
                @csrf

                <div class="form-group">
                    <label for="name">{{ __('Nama Klub') }}</label>
                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                    @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="city">{{ __('Kota Klub') }}</label>
                    <input id="city" type="text" class="form-control @error('city') is-invalid @enderror" name="city" value="{{ old('city') }}" required autocomplete="city">
                    @error('city')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <button type="submit" class="btn btn-primary">{{ __('Simpan') }}</button>
            </form>
        </div>
    </div>
@endsection
