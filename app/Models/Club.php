<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Club extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'city',
    ];

    public function matchesAsHome()
    {
        return $this->hasMany(Match::class, 'home_club_id');
    }

    public function matchesAsAway()
    {
        return $this->hasMany(Match::class, 'away_club_id');
    }

    public function getTotalPointsAttribute()
    {
        return $this->matchesAsHome->sum(function ($match) {
            if ($match->home_goals > $match->away_goals) {
                return 3;
            } elseif ($match->home_goals === $match->away_goals) {
                return 1;
            } else {
                return 0;
            }
        }) + $this->matchesAsAway->sum(function ($match) {
            if ($match->away_goals > $match->home_goals) {
                return 3;
            } elseif ($match->away_goals === $match->home_goals) {
                return 1;
            } else {
                return 0;
            }
        });
    }

    public function getTotalMatchesAttribute()
    {
        return $this->matchesAsHome->count() + $this->matchesAsAway->count();
    }

    public function getTotalWinsAttribute()
    {
        return $this->matchesAsHome->filter(function ($match) {
            return $match->home_goals > $match->away_goals;
        })->count() + $this->matchesAsAway->filter(function ($match) {
            return $match->away_goals > $match->home_goals;
        })->count();
    }

    public function getTotalDrawsAttribute()
    {
        return $this->matchesAsHome->filter(function ($match) {
            return $match->home_goals === $match->away_goals;
        })->count() + $this->matchesAsAway->filter(function ($match) {
            return $match->away_goals === $match->home_goals;
        })->count();
    }

    public function getTotalLossesAttribute()
    {
        return $this->matchesAsHome->filter(function ($match) {
            return $match->home_goals < $match->away_goals;
        })->count() + $this->matchesAsAway->filter(function ($match) {
            return $match->away_goals < $match->home_goals;
        })->count();
    }

    public function getTotalGoalsForAttribute()
    {
        return $this->matchesAsHome->sum('home_goals') + $this->matchesAsAway->sum('away_goals');
    }

    public function getTotalGoalsAgainstAttribute()
    {
        return $this->matchesAsHome->sum('away_goals') + $this->matchesAsAway->sum('home_goals');
    }

    public function getTotalGoalDifferenceAttribute()
    {
        return $this->getTotalGoalsForAttribute() - $this->getTotalGoalsAgainstAttribute();
    }
}
